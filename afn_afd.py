# list = ['larry', 'curly', 'moe']
# list.append('shemp')
# list.insert(0, 'xxx')
# print(list.index('curly'))

print('\nOperaciones disponibles\n')
print('Mover -----------------> m')
print('Concatenación ---------> -')
print('Unión -----------------> u')
print('Cerradura positiva ----> +')
print('Cerradura de Kleene ---> *\n')

# Este estado avisará en qué estado se encuentra el usuario
estado = 0
automata_final = []
estados_nuevos = []
resultados_de_mover = []

# función recursiva que maneja las operaciones
def operaciones(operacion = None, kleene=False):
    global estado

    operacion = input('In op [{0}]> '.format(estado))

    # si la operación introducida es -1
    # retorna el autómata
    if operacion == '-1':
        return automata_final
    elif operacion == 'm':
        print('\nMover\n')
        simbolo = input('Introduce tu letra> ')
        
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [estado, None],
            'simbolo': simbolo,
            'sub_estado_2': estado + 1,
            'return_to': None
        })

        estado += 1

    elif operacion == '-':
        print('\nConcatenación\n')        

        return operaciones(operacion)
        # while True:
        #     simbolo = input('Introduce tu letra> ')
            
        #     if simbolo == '0':
        #         break
            
        #     automata_final.append({
        #         'go_to': None,
        #         'sub_estado_1': [estado, None],
        #         'simbolo': simbolo,
        #         'sub_estado_2': estado + 1,
        #         'return_to': None
        #     })

        #     estado += 1

    elif operacion == 'u':
        print('\nUnión\n')

        union_estado_1 = estado

        # parte de arriba
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [union_estado_1, None],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': None
        })
        estado += 1

        operaciones(operacion)

        union_estado_2 = estado
        
        # parte de abajo
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [union_estado_1, None],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': None
        })
        estado += 1

        operaciones(operacion)

        # parte final
        if kleene:
            automata_final.append({
            'go_to': None,
            'sub_estado_1': [union_estado_2, estado],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': union_estado_1
            })
        else:
            automata_final.append({
                'go_to': None,
                'sub_estado_1': [union_estado_2, estado],
                'simbolo': 'E',
                'sub_estado_2': estado + 1,
                'return_to': None
            })

        estado += 1

    elif operacion == '+':
        print('\nCerradura +\n')

        inicial = estado

        # inicial
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [estado, None],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': None
        })
        estado += 1

        operaciones(operacion, kleene=True)

        # final
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [estado, None],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': None
        })
        estado += 1

    elif operacion == '*':
        print('\nCerradura *\n')

        inicial = estado

        # inicial
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [estado, None],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': None
        })
        estado += 1

        operaciones(operacion, kleene=True)

        # final
        automata_final.append({
            'go_to': None,
            'sub_estado_1': [estado, None],
            'simbolo': 'E',
            'sub_estado_2': estado + 1,
            'return_to': None
        })

        # actualización del inicial
        automata_final[inicial].update(go_to = estado + 1)
        estado += 1

    return operaciones(operacion)

# función que revisa los caminos que toma el estado inicial (0)
def e_cerradura_s(automata):
    global estado
    # estado_en_operacion = [1, None] # en qué estado estamos actualmente revisando
    estados = [0]

    if automata[0]['go_to']:
        estados.append(automata[0]['go_to'])

    for i in range(estado):
        if automata[i]['simbolo'] == 'E':
            if automata[i]['sub_estado_1'][0] in estados:
                # TODO: agregar el go_to y el return_to del actual
                estados.append(automata[i]['sub_estado_2'])

    return { 'a': estados }

def e_cerradura_t(automata, resultados):
    global estado
    estados = resultados
    revisados = []
    pasada = 1
    pasadas = 1

    # for i in range(len(estados)):
    #     print(estados[i])

    while pasada <= pasadas:
        for i in range(estado):
            if automata[i]['simbolo'] == 'E':
                if automata[i]['sub_estado_1'][0] in estados:
                    estados.append(automata[i]['sub_estado_2'])
                    if automata[i]['go_to'] != None and automata[i]['go_to'] not in revisados:
                        estados.append(automata[i]['go_to'])
                        revisados.append(automata[i]['go_to'])
                        pasadas += 1
                    if automata[i]['return_to'] != None and automata[i]['go_to'] not in revisados:
                        estados.append(automata[i]['return_to'])
                        revisados.append(automata[i]['go_to'])
                        pasadas += 1
                if automata[i]['sub_estado_1'][1] in estados:
                    if automata[i]['sub_estado_2'] not in estados:
                        estados.append(automata[i]['sub_estado_2'])
        pasada += 1

    print(estados)
    return { 'b': estados }

def mover(automata, T, simbolo):
    global estado
    resultados = []
    
    for i in range(estado):
        if automata[i]['simbolo'] == simbolo:
            if automata[i]['sub_estado_1'][0] in T:
                resultados.append(automata[i]['sub_estado_2'])

    print(resultados)
    return resultados

########## test starts here ###########
estado = 10

automata_final = [
    {'go_to': 7, 'sub_estado_1': [0, None], 'simbolo': 'E', 'sub_estado_2': 1, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [1, None], 'simbolo': 'E', 'sub_estado_2': 2, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [2, None], 'simbolo': 'a', 'sub_estado_2': 3, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [1, None], 'simbolo': 'E', 'sub_estado_2': 4, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [4, None], 'simbolo': 'b', 'sub_estado_2': 5, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [3, 5], 'simbolo': 'E', 'sub_estado_2': 6, 'return_to': 1},
    {'go_to': None, 'sub_estado_1': [6, None], 'simbolo': 'E', 'sub_estado_2': 7, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [7, None], 'simbolo': 'a', 'sub_estado_2': 8, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [8, None], 'simbolo': 'b', 'sub_estado_2': 9, 'return_to': None},
    {'go_to': None, 'sub_estado_1': [9, None], 'simbolo': 'b', 'sub_estado_2': 10, 'return_to': None}
]

for automata in automata_final:
    print('{0}({1}{2})-{3}-({4}) {5}'.format(
        'go_to={0} '.format(automata['go_to']) if automata['go_to'] else '',
        automata['sub_estado_1'][0],
        ', {0}'.format(automata['sub_estado_1'][1]) if automata['sub_estado_1'][1] else '',
        automata['simbolo'],
        automata['sub_estado_2'],
        'return_to={0}'.format(automata['return_to']) if automata['return_to'] else ''
    ))

estados_nuevos.append(e_cerradura_s(automata_final))
resultados_de_mover = mover(automata_final, estados_nuevos[0]['a'], 'a')
e_cerradura_t(automata_final, resultados_de_mover)
########## test ends here ###########

# operaciones()

# print('\nTabla de transición\n')
# for automata in automata_final:
#     print('{0}({1}{2})-{3}-({4}) {5}'.format(
#         'go_to={0} '.format(automata['go_to']) if automata['go_to'] else '',
#         automata['sub_estado_1'][0],
#         ', {0}'.format(automata['sub_estado_1'][1]) if automata['sub_estado_1'][1] else '',
#         automata['simbolo'],
#         automata['sub_estado_2'],
#         'return_to={0}'.format(automata['return_to']) if automata['return_to'] else ''
#     ))
# print('\nEstado de aceptación [{0}]'.format(estado))

# e_cerradura_s(automata_final)